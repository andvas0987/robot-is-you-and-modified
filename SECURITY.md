# Security Policy

The master branch receives topical security updates.

To report a vulnerability, submit an issue.
For more significant issues, or direct conversation, contact me in on Discord (Niki4tap#6227).

Issues are typically patched within a week, depending on the severity.
