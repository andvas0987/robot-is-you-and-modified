<#
	Robot is you and modified discord bot, made by RocketRace, modified by Niki4tap.
	Creates some folders and files that are necessary to run the bot. 
#>

$first = $Args[0]

if ("$($first)" <# Dirty workaround to be honest, but seems like it's working #> -eq "") {
	Write-Error "No argument supplied"
	Write-Output "Usage: make.ps1 <token>"
	exit
}

# Making cache directory
mkdir cache/

# Filling cache
Write-Output "[]" > cache/blacklist.json
Write-Output "{}" > cache/customlevels.json
Write-Output '{"identifies": [], "resumes": []}' > cache/debug.json
Write-Output "{}" > cache/leveldata.json
Write-Output "{}" > cache/tiledata.json

# Creating auth.json
Write-Output "{`"token`": `"$($first)`"}" > config/auth.json

# Creating target/
mkdir target/

# Filling target/
mkdir -p target/letters/small
mkdir target/letters/big
mkdir target/letters/thick
mkdir -p target/render/custom

Write-Output "Basic setup is done. It is highly recommended to issue 'loaddata' command on bot's first startup."